package com.twuc.songapplication.domain;

import com.twuc.songapplication.contract.CreateProductRequest;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @Column(name = "productCode", nullable = false, length = 15)
    private String code;

    @Column(name = "productName", nullable = false, length = 70)
    private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "productLine")
    private ProductLine productLine;

    @Column(name = "productScale", nullable = false, length = 10)
    private String scale;

    @Column(name = "productVendor", nullable = false, length = 50)
    private String vendor;

    @Column(name = "productDescription", nullable = false, columnDefinition = "text")
    private String description;

    @Column(name = "quantityInStock", nullable = false)
    private Short quantityInStock;

    @Column(name = "buyPrice", nullable = false)
    private BigDecimal buyPrice;

    @Column(name = "MSRP", nullable = false)
    private BigDecimal MSRP;

    public Product(CreateProductRequest product) {
        this.name = product.getName();
        this.code = product.getCode();
        this.buyPrice = product.getBuyPrice();
        this.description = product.getDescription();
        this.MSRP = product.getMSRP();
        this.productLine = new ProductLine(product.getProductLine());
        this.vendor = product.getVendor();
        this.scale = product.getScale();
        this.quantityInStock = product.getQuantityInStock();
    }

    public Product() {
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public ProductLine getProductLine() {
        return productLine;
    }

    public String getScale() {
        return scale;
    }

    public String getVendor() {
        return vendor;
    }

    public String getDescription() {
        return description;
    }

    public Short getQuantityInStock() {
        return quantityInStock;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public BigDecimal getMSRP() {
        return MSRP;
    }
}
