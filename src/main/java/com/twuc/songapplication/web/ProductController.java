package com.twuc.songapplication.web;

import com.twuc.songapplication.contract.CreateProductRequest;
import com.twuc.songapplication.domain.Product;
import com.twuc.songapplication.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private ProductRepository repository;

    @GetMapping("/products")
    Page<CreateProductRequest> getProducts(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "name") String sort,
            @RequestParam(defaultValue = "asc") String direction
    ) {
        return repository.findAll(PageRequest
                .of(page, size, Sort.Direction.fromString(direction), sort))
                .map(product -> new CreateProductRequest(product));
    }

    @PostMapping("/products")
    void createProduct(@RequestBody @Valid CreateProductRequest product) {
        repository.save(new Product(product));
    }
}
