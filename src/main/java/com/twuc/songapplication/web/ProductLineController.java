package com.twuc.songapplication.web;

import com.twuc.songapplication.domain.ProductLine;
import com.twuc.songapplication.domain.ProductLineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ProductLineController {

    @Autowired
    private ProductLineRepository repository;

    @GetMapping("/product-lines")
    Page<ProductLine> getProductLines(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "name") String sort,
            @RequestParam(defaultValue = "asc") String direction
    ) {
        return repository.findAll(
                PageRequest.of(page, size, Sort.Direction.fromString(direction), sort));
    }

    @PostMapping("/product-lines")
    void createProductLine(@RequestBody ProductLine productLine) {
        repository.save(productLine);
    }
}
