package com.twuc.songapplication.domain;

import javax.persistence.*;

@Entity
@Table(name = "productlines")
public class ProductLine {
    @Id
    @Column(name = "productLine", length = 50)
    private String name;
    @Column(name = "textDescription")
    private String description;

    public ProductLine(String productLine, String textDescription) {
        this.name = productLine;
        this.description = textDescription;
    }

    public ProductLine(String name) {
        this.name = name;
    }

    public ProductLine() {
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
