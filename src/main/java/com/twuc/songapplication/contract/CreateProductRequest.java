package com.twuc.songapplication.contract;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.twuc.songapplication.domain.Product;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class CreateProductRequest {
    @NotNull
    @Size(min = 1, max = 15)
    private String code;

    @NotNull
    @Size(min = 1, max = 70)
    private String name;

    @NotNull
    @Size(min = 1, max = 50)
    private String productLine;

    @NotNull
    @Size(min = 1, max = 10)
    private String scale;

    @NotNull
    @Size(min = 1, max = 50)
    private String vendor;

    @NotNull
    private String description;

    @NotNull
    private Short quantityInStock;

    @NotNull
    private BigDecimal buyPrice;

    @NotNull
    @JsonProperty
    private BigDecimal MSRP;

    public String getCode() {
        return code;
    }

    public CreateProductRequest(Product product) {
        this.code = product.getCode();
        this.name = product.getName();
        this.buyPrice = product.getBuyPrice();
        this.description = product.getDescription();
        this.MSRP = product.getMSRP();
        this.vendor = product.getVendor();
        this.productLine = product.getProductLine().getName();
        this.scale = product.getScale();
        this.quantityInStock = product.getQuantityInStock();
    }

    public CreateProductRequest() {
    }

    public String getName() {
        return name;
    }

    public String getProductLine() {
        return productLine;
    }

    public String getScale() {
        return scale;
    }

    public String getVendor() {
        return vendor;
    }

    public String getDescription() {
        return description;
    }

    public Short getQuantityInStock() {
        return quantityInStock;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    @JsonIgnore
    public BigDecimal getMSRP() {
        return MSRP;
    }
}
